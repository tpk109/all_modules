<?php
/**
* class_convert_datum
*
*  ##############################################################################
*  # class_convert_datum.php
*  # Klasse zur Verifizierung von Benutzerangaben und zum Erleichterten Abfangen
*  # möglicher Sicherheitslücken.
*  # Copyright (C) 2004 Daniel de West
*  #
*  # class_convert_datum.php ist freie Software; Sie dürfen sie unter den Bedingungen der
*  # GNU Lesser General Public License, wie von der Free Software Foundation
*  # veröffentlicht, weiterverteilen und/oder modifizieren; entweder gemäß
*  # Version 2.1 der Lizenz oder (nach Ihrer Option) jeder späteren Version.
*  #
*  # class_convert_datum.php wird in der Hoffnung weiterverbreitet, daß sie nützlich
*  # sein wird, jedoch OHNE IRGENDEINE GARANTIE, auch ohne die implizierte Garantie
*  # der MARKTREIFE oder der VERWENDBARKEIT FÜR EINEN BESTIMMTEN ZWECK. Mehr Details
*  # finden Sie in der GNU Lesser General Public License.
*  #
*  # Sie sollten eine Kopie der GNU Lesser General Public License zusammen mit
*  # dieser Bibliothek/diesem Programm erhalten haben; falls nicht, schreiben Sie
*  # an die Free Software Foundation, Inc., 59 Temple Place, Suite 330, Boston,
*  # MA 02111-1307, USA.
*  #
*  # Das Regionale Rechenzentrum Erlangen (http://www.rrze.uni-erlangen.de)
*  # erhebt keinen urheberechtlichen Anspruch auf das von
*  #       Daniel de West
*  # geschriebene Programm.
*  ##############################################################################
*
* @author Daniel de West
*
* @version $Revision: 1.15 $
* Wichtige Logs
*
$Log: class_convert_datum.php,v $
Revision 1.15  2005/03/10 09:06:23  unrz23
Es kann jetzt direkt eine Sprache übergeben werden und die Set-Locale-Funktion intern aufgerufen

Revision 1.14  2005/01/11 10:19:34  unrz23
Einfügung der GPL-Lizenz

Revision 1.13  2004/12/20 14:31:27  unrz23
Einbinden der Revisions-Logs in Class-Defintion


Revision 1.11  2004/12/20 11:23:40  unrz23
- Verbesserte Darstellung der Veränderungen

Revision 1.3  2004/12/20 11:14:0  unrz23
- Verbesserte Erkennung des Datums
- Möglichkeit eigene Zusatzfunktionen zu schreiben vereinfacht

Revision 1.2  2004/09/15 11:24:0  unrz23
- Expansion tags dazugefuegt (Id, Log)

Revision 1.1  2004/09/15 10:00:0  unrz23
- Initial Revision
*/
date_default_timezone_set('Europe/Berlin');
class convert_datum {
        /**
        * convert_datum::locale
        * @var array $locale Angabe der für die Berechnungen gültigen Lokalen Angaben
        * Da auf unterschiedlichen Betriebssystemen unterschiedliche Angaben möglich sind,
        * sollten verschiedene Werte als Array übergeben werden. Es wird dann automatisch
        * ein gültiger Wert genommen.
        * Default: array('de_DE@euro', 'de_DE', 'de', 'ge')
        *
        * @author Daniel de West
        * @access private
        */

        var $locale=array('de_DE@euro', 'de_DE', 'de', 'ge', "deu_deu");
	
	

        /**
        * convert_datum::set_locale
        *
        * Mit dieser Funktion kann ein neuer Wert für convert_datum::locale gesetzt werden.
        *
        * @author Daniel de West
        * @access public
        *
        * @param array $new_locale Ein Array mit den neuen Locale-Werten (z. B. array('de_DE@euro', 'de_DE', 'de', 'ge'))
        * @return boolean $ergebnis Gibt zurück ob das Setzen des neuen Locale-Wertes funktioniert hat (TRUE) oder nicht (FALSE)
        */
        function set_locale($new_locale) {
                // Überprüfung, ob es sich um ein Array handelt
                if (!is_array($new_locale)) $new_locale=array($new_locale);
                // Setzen des neuen Locale-Wertes
                $this->locale=$new_locale;
                if (!setlocale (LC_TIME, $this->locale)) {
					return FALSE;
				} else {
					return TRUE;
				}
        }

        /**
        * convert_datum::set_locale_language
        *
        * Mit dieser Funktion kann ein neuer Wert für convert_datum::locale gesetzt werden.
		* Es handelt sich um eine Vereinfachung von convert_datum::set_locale, da hier nur der
		* Name der Sprache angegeben werden muss.
        *
        * @author Daniel de West
        * @access public
        *
        * @param array $new_locale Der Englische Name der zu setzenden Sprache. Möglich sind:
		* - german (default)
		* - french
		* - italian
        * @return boolean $ergebnis Gibt zurück ob das Setzen des neuen Locale-Wertes funktioniert hat (TRUE) oder nicht (FALSE)
        */
		function set_locale_language($lang) {
			switch ($lang) {
				case french:
					$lang_array=array("fra","fre","fr","FR","fr_FR","fr_FR.utf8","fr_FR@euro");
					break;
				case italian:
					$lang_array=array("ita","it","IT","it_IT","it_IT","it_IT.utf8","it_IT@euro");
					break;
				case german:
					$lang_array=array('de_DE@euro', 'de_DE', 'de', 'ge','de_DE.utf8');
			}
			if (!is_array($lang_array)) die("Es wurde ein ungültiger Sprachname übergeben.");
			return $this->set_locale($lang_array);
		}

        /**
        * convert_datum::get_stamp
        *
        * Wandelt eine beliebige Datumseingabe in einen UNIX-Timestamp um. Möglich sind:
        * - TT.MM.JJJJ
		* - T.MM.JJJJ
		* - T.M.JJJJ
		* - TT.MM.JJ
		* - T.MM.JJ
		* - T.M.JJ
        * - JJJJ-MM-TT
        * - MM/TT/JJJJ
        * - Unix-Timestamp
        *
        * @access public
		* @since 20.12.2004
        * @author Daniel de West
        *
        * @param string $d Eine Datumsangabe in einem der folgenden Formate:
		* -
        * @return mixed $stamp Ein UNIX-Timestamp oder FALSE
        */
		function convert2stamp ($d) {
			// Entfernen von Leerzeichen davor und danach
			trim($d);
			// Ausgang ist deutsches Datum
				// 01.12.2004 oder 01.12.04
				if (preg_match("/^[0-9]{2}\.[0-9]{2}\.[0-9]{2,4}$/",$d)) return mktime(0,0,0,substr($d,3,2),substr($d,0,2),substr($d,6));
				// 1.12.2004 oder 1.12.04
				if (preg_match("/^[0-9]{1}\.[0-9]{2}\.[0-9]{2,4}$/",$d)) return mktime(0,0,0,substr($d,2,2),substr($d,0,1),substr($d,5));
				// 12.1.2004 oder 12.1.04
				if (preg_match("/^[0-9]{2}\.[0-9]{1}\.[0-9]{2,4}$/",$d)) return mktime(0,0,0,substr($d,3,1),substr($d,0,2),substr($d,5));
				// 1.1.2004 oder 1.1.04
				if (preg_match("/^[0-9]{1}\.[0-9]{1}\.[0-9]{2,4}$/",$d)) return mktime(0,0,0,substr($d,2,1),substr($d,0,1),substr($d,4));


			// Ausgang ist US Datum
			if (preg_match("/^[0-9]{4}-[0-9]{2}-[0-9]{2}$/",$d)) return mktime(0,0,0,substr($d,5,2),substr($d,8,2),substr($d,0,4));

			// Ausgang ist ein Firebird-Datum ohne Uhrzeit
			if (preg_match("/^[0-9]{2}\/[0-9]{2}\/[0-9]{4}$/",$d)) {
				return mktime(0,0,0,substr($d,0,2),substr($d,3,2),substr($d,6,4));
			}
			// Ausgang ist ein Firebird-Datum mit Uhrzeit (TIMESTAMP)
			if (preg_match("/^[0-9]{2}\/[0-9]{2}\/[0-9]{4} [0-9]{2}:[0-9]{2}:[0-9]{2}$/",$d)) {
				return mktime(substr($d,11,2),substr($d,14,2),substr($d,17,2),substr($d,0,2),substr($d,3,2),substr($d,6,4));
			}

			// Ausgang ist ein Timestamp
			if (is_numeric($d)) return $d;

			// Ausgang ist nicht direkt zuzuordnen
			return FALSE;
		}

        /**
        * convert_datum::convert
        *
        * Methode zur Umwandlung von Datumsformaten
        * Zunächst werden intern alle möglichen Eingangsdatumsformate in einen Unix-Timestamp umgewandelt.
        * Darauf basierend kann das gewünschte Ausgabedatum gewählt werden.
        * Achtung: Es können (aufgrund der Beschränkungen des Unix-Timestamp) keine Daten vor dem 1.1.1970 oder nach dem 1.1.2038 verarbeitet werden.
        *
        * @access public
        * @author Daniel de West
        *
        * @param string $d Das Datum in der ursprünglichen Form. Möglich sind die unter convert2stamp angegebenen Formate
        * @param string $ausgabe Angabe eines Ausgabeformats. Möglich sind:
        * - "d" Deutsches Format: TT.MM.JJJJ
        * - "us" US-Format: JJJJ-MM-TT
        * - "f" Firebird-Format: MM/TT/JJJJ/
        * - "dz" Datum (deutsch) und Zeit: TT.MM.JJJJ Stunden:Minuten:Seanbieter
        * - "sms" Zeit in Stunden:Minuten:Seanbieter
        * - "z" Unix-Timestamp
        * - "t" Tag
        * - "m" Monat
        * - "j" Jahr (vierstellig)
        * - "wt0" Wochentag als Zahl von 0 (Sonntag) bis 6 (Samstag)
        * - "wt" Abgekürzter Wochentag: z. B. Mo, Di
        * - "wtd" Abgekürzter Wochentag Komma und deutsches Datum z. B. Mo, 01.04.2004
        * - "mt" Abkürzung Monat
        * - "monat" Monat (Langform)
        * - "last" Anzahl der Tage in diesem Monat
        * - "kw" Kalenderwoche des Jahres, beginnend mit dem 1. Sonntag des Jahres
        * - "kw_montag" Kalenderwoche des Jahres, beginnend mit dem 1. Montag des Jahres
        * - "kw_iso" Kalenderwoche des Jahres, immer 2-stellig, Die erste Woche des Jahres mit Montag als Starttag und mindestens 4 Wochentagen im aktuellen Jahr
        * @return string $ausgabe Das umgewandelte Datum im gewünschten Ausgabeformat
        */
        function convert($d,$ausgabe) {
				// Setzen der lokalen Einstellung für die Ausgaben
				// falls "gediet" wird -> var locale braucht noch einen anderen wert für das system
                if (!setlocale (LC_TIME, $this->locale)) die("Es wurde ein ungültiger Wert für setlocale  in class_conver_datum übergeben");

				// Umwandeln der Datumsangabe in einen Timestamp
				$stamp=$this->convert2stamp($d);

                if ($ausgabe=="d") return date("d.m.Y",$stamp); // Deutsches Format d
                if ($ausgabe=="us") return date("Y-m-d",$stamp); // US-Format us
                if ($ausgabe=="f") return date("m/d/Y",$stamp); // Firebird-Format f
                if ($ausgabe=="dz") return date("d.m.Y H:i:s",$stamp); // Datum (deutsch) und Zeit
                if ($ausgabe=="sms") return date("H:i:s",$stamp); // Zeit in Stunden:Minuten:Seanbieter
                if ($ausgabe=="z") return $stamp; // Zeitstempel z
                if ($ausgabe=="t") return date("d",$stamp); // Tag t
                if ($ausgabe=="m") return date("m",$stamp); // Monat m
                if ($ausgabe=="j") return date("Y",$stamp); // Jahr (vierstellig) j
                if ($ausgabe=="j2") return str_replace("20","",date("Y",$stamp)); // Jahr (vierstellig) j
                if ($ausgabe=="wt0") return date("w",$stamp); // Wochentag als Zahl von 0 (Sonntag) bis 6 (Samstag)
                if ($ausgabe=="wt") return strftime("%a",$stamp); // Wochentag: wt z. B. Mo, Di
                if ($ausgabe=="wtd") return strftime("%a, %d.%m.%Y",$stamp); // z. B. Mo, 01.04.2004
                if ($ausgabe=="mt") return strftime("%b",$stamp); // Abkürzung Monat
                if ($ausgabe=="monat") return strftime("%B",$stamp); // Abkürzung Monat
                if ($ausgabe=="kw") return strftime("%U",$stamp); // Kalenderwoche (beginnend mit dem ersten Sonntag des Jahres)
                if ($ausgabe=="kw_iso") return strftime("%V",$stamp); // Kalenderwoche (nach ISO 8601:1988)
                if ($ausgabe=="kw_montag") return strftime("%W",$stamp); // Kalenderwoche (beginnend mit dem ersten Montag des Jahres)
				if (method_exists($this,$ausgabe)) return $this->$ausgabe($stamp);
				else die ("Konfigurationsfehler: Die Funktion $ausgabe existiert nicht");
		}

        /**
        * convert_datum::last
        *
        * Gibt den letzten Tag eines Monats eines Timestamps zurück
        *
        * @access public
		* @since 20.12.2004
        * @author Daniel de West
        *
        * @param string $stamp Ein UNIX-Timestamp
        * @return string $ausgabe Der letzte Tag des Monats des Timestamps
        */
		function last ($stamp) {
			return date("t",$stamp); // Anzahl der Tage in diesem Monat
		}
    
            /**
        * convert_datum::monatsname
        *
        * Gibt deutsche Monatsnamen zurueck
        * 
        * @access public
		    * @since 21.11.2008
        * @author Thomas Kratz
        *
        * @param string monat in form 01-12 = return von convert($d,"m")
        * @return string Monat
        */
		function monatsname ($monat) {
			if($monat=="01") return "Januar";
      if($monat=="02") return "Februar";
      if($monat=="03") return "M&auml;rz";
      if($monat=="04") return "April";
      if($monat=="05") return "Mai";
      if($monat=="06") return "Juni";
      if($monat=="07") return "Juli";
      if($monat=="08") return "August";
      if($monat=="09") return "September";
      if($monat=="10") return "Oktober";
      if($monat=="11") return "November";
      if($monat=="12") return "Dezember";
      
		}

} // Ende der Class

?>