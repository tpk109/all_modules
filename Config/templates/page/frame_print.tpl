<!DOCTYPE html>
<html lang="de">
<head>
    <link href="https://fonts.googleapis.com/css?family=ABeeZee:400,400i|Raleway:400,700,900" rel="stylesheet">
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="../../favicon.ico">

    <title>{{title_tag}}</title>

    <!-- Bootstrap core CSS -->
    <link href="<?php echo $this->http; ?>others_libs/css/bootstrap/css/bootstrap.min.css" rel="stylesheet">

    <!-- Font Awesome -->
    <script src="<?php echo $this->http; ?>others_libs/js/fontawesome-all.min.js" type="text/javascript"></script>


    <!-- jquery -->
    <script src="<?php echo $this->http; ?>others_libs/js/jquery.js" type="text/javascript"></script>

    <!-- PageOs js.js -->
    <script src="<?php echo $this->http; ?>lib/js.js" type="text/javascript"></script>

    <!-- cke -->
    <script src="<?php echo $this->http; ?>others_libs/ckeditor/ckeditor.js"></script>

    <?php if(@$this->include_in_page->async): ?>
    <script src="<?php echo $this->http; ?>others_libs/js/async.js" type="text/javascript"></script>
    <?php endif; ?>

    <!-- paths -->
    <script>
      var json_configs = '<?php echo $json_configs; ?>';
      var json_configs_obj = jQuery.parseJSON('<?php echo $json_configs; ?>');
    </script>


    <!-- css -->
    <link href="<?php echo $this->http; ?>lib\formit\others_libs\jquery-ui-1.12.1.custom/jquery-ui.structure.min.css" type="text/css" rel="stylesheet" />
    <link href="<?php echo $this->http; ?>lib\formit\others_libs\jquery-ui-1.12.1.custom/jquery-ui.css" type="text/css" rel="stylesheet" />

    <!-- bxslider -->
    <link href="<?php echo $this->http; ?>lib\formit\others_libs\bxslider/jquery.bxslider.css" rel="stylesheet" />

    <link href="<?php echo $this->http_project; ?>templates/css/all_print.css" rel="stylesheet">

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->


</head>

<body <?php echo @$style; ?> role="document" class='<?php echo (@$body_class)? @$body_class : $this->file; ?>'>
{{developer}}
<?php echo $html; ?>

<!-- jquery.filer -->
<script src="<?php echo $this->http; ?>lib/formit/others_libs/jQuery.filer-master/js/jquery.filer.js?v=1.0.5"></script>
<script src="<?php echo $this->http; ?>lib/formit/others_libs/jQuery.filer-master/js/custom.js?v=1.0.5"></script>
<link href="<?php echo $this->http; ?>lib/formit/others_libs/jQuery.filer-master/css/jquery.filer.css" type="text/css" rel="stylesheet" />
<link href="<?php echo $this->http; ?>lib/formit/others_libs/jQuery.filer-master/css/themes/jquery.filer-dragdropbox-theme.css" type="text/css" rel="stylesheet" />
<!-- jQuery ui -->
<script src="<?php echo $this->http; ?>lib\formit\others_libs\jquery-ui-1.12.1.custom/jquery-ui.min.js"></script>
<!-- bxslider -->
<script src="<?php echo $this->http; ?>lib\formit\others_libs\bxslider/jquery.bxslider.min.js"></script>

<!-- lightbox -->
<script src="<?php echo $this->http; ?>my_libs\js\js_stuff.js" type="text/javascript"></script>
<script src="<?php echo $this->http; ?>others_libs/css/bootstrap/js/bootstrap.min.js"></script>

<!-- js.js -->
<script src="<?php echo $this->http_project; ?>libs/js/js.js" type="text/javascript"></script>

<!-- IE10 viewport hack for Surface/desktop Windows 8 bug -->
<script src="<?php echo $this->http; ?>others_libs/css/bootstrap/js/ie10.js"></script>

<!-- BACKGROUND LIGHTBOX -->
<div id='bgLIGHTBOX' class='fadedOut'></div>

<div id='LIGHTBOX' class='fadedOut rund'>
    <div id='inLIGHTBOX'></div>
</div>

<?php if(@$this->include_in_page->sticky): ?>
<script src="<?php echo $this->http; ?>others_libs/js/jquery.sticky.js"
        defer></script>
<?php endif; ?>


<script type="text/javascript">
  <?php echo @$this->add_js; ?>

  $(document).ready(function(){
  <?php echo @$this->add_js_document_ready; ?>

  });// Break above }); important!!

</script>


</body>
</html>