<?php

/**
 * LOKAL
 */

if(@$_SERVER['SystemRoot'] == 'C:\Windows' || @$_SERVER['SystemRoot'] == 'C:\WINDOWS'){
  define('LOCAL', true);
  define('TEST', true);
}
else
  define('LOCAL', false);

/**
 * Autoload
 */
spl_autoload_register(function ($name) {

  GLOBAL $name_project_folder;

  if(LOCAL === true)
    $back_to_PageOs = '/../../';
  else $back_to_PageOs = '/../../';

  # When class_ajax_Frontend.php is called, don't include it again
  if($name == 'ajax_'.$name_project_folder)
    $foo = 3;
  elseif(@include ROOT_PROJECT."/class_$name.php")
  {$foo = 1;}
  # in Logos
  elseif (@include realpath(dirname(__FILE__)) . "/class_$name.php")
    $foo = 1;
  elseif (@include realpath(dirname(__FILE__)) . "/libs/class_$name.php")
    $foo = 1;
  # in PageOs
  elseif(@include realpath(dirname(__FILE__).$back_to_PageOs)."/PageOs/lib/class_$name.php")
    $foo = 2;
  elseif(@include realpath(dirname(__FILE__).$back_to_PageOs)."/PageOs/lib/formit/class_$name.php")
    $foo = 3;

//  echo "<span style='color: red'><br>TEST :: " . basename(__FILE__) . " (" . __LINE__ . ") -> </span>";
//  echo $name;
});