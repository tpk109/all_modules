<?php

class QE{
  static $B;
  public static function init($B){
    self::$B = $B;
  }

  public static function articles_name($id){
    $sql = "SELECT * FROM `articles` WHERE `id`='$id'";
    $ROW = DB::data_rows_o_0($sql);
    return "$ROW->Bezeichnung";
  }
  
  public static function employee($id){
    $sql = "SELECT * FROM `employees` WHERE `id`='$id'";
    return DB::data_rows_o_0($sql);
  }

  public static function employee_name_pn($personnel_number){
    $sql = "SELECT * FROM `employees` WHERE `personnel_number`='$personnel_number'";
    $ROW = DB::data_rows_o_0($sql);
    return "$ROW->firstname $ROW->lastname";
  }

  public static function employee_name($id){
    $sql = "SELECT * FROM `employees` WHERE `id`='$id'";
    $ROW = DB::data_rows_o_0($sql);
    return "$ROW->firstname $ROW->lastname";
  }

  static function get_exceptions_field($d){

    # table: articles
    $table_basis = $d['table_basis'];

    # articles_id. in both tables (this and exception) the same!
    $fieldname_link_to_table_basis = $table_basis.'_id';

    # value of arcticles_id
    $table_basis_id = $d['table_basis_id'];

    # table name exceptions
    $table_exceptions = $table_basis.'_exceptions';

    $table_exceptions = 'articles_exceptions';

    # field from table_basis to show
    $field_basis = $d['field_basis'];

    /*
     * Only the top finding is used!!
     * TODO: Historie, nicht Curdate verwenden sondern Datum von Datensatz um historische Preise zu erhalten
     */
    $sql = "
        SELECT `$field_basis` FROM `$table_exceptions` WHERE
        `$fieldname_link_to_table_basis`='$table_basis_id' AND
        (!(`from` > '')  OR `from`<=CURDATE()) AND
        (`till` IS NULL OR `till` = '' OR `till`>=CURDATE()) AND
        (`from` IS NOT NULL OR `till` IS NOT NULL)
        ORDER BY `id` DESC
    ";
    $exceptional_value = DB::vf($sql);
    if($exceptional_value != '')
      return self::$B->point_comma($exceptional_value);
    else{
      $sql = "SELECT `$field_basis` FROM `$table_basis` WHERE `id`='$table_basis_id'";
      return  self::$B->point_comma(DB::vf($sql));
    }
  }

  static function exep_articles_VK($table_basis_id, $date = ''){
    $funcname_exploded = explode('_',__FUNCTION__);
    $d['table_basis'] = $funcname_exploded['1'];
    $d['field_basis'] = $funcname_exploded['2'];
    $d['table_basis_id'] = $table_basis_id;
    $d['date'] = $date;

    return self::get_exceptions_field($d);
  }
}