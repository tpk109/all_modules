<?php
/*
 * rootroot, should be renamed to something else
 * needed for symlinks
 */
$this->rootroot = realpath(dirname(__FILE__).'/../').'/';

/*
 * Symlink
 */
if(LOCAL)
  $this->rootroot_symlink = realpath(dirname(__FILE__).'/../').'\\'; // needs backslash for windows
else{
  $this->rootroot_symlink = realpath(dirname(__FILE__).'/../').'/';
  $this->path_to_PageOs_server = $this->rootroot.'PageOs';
}

# Ajax base in Project Path
$this->path_ajax_base_in_project = '/';


/**
 * http-link to ajax_base_in_project is different when called from Contao
 */
$this->ajax_base_in_project = (defined('CONTAO'))? "PAGEOS/{$d['name_project_folder']}/" : '';

# basis_folder = Projects Basis = basic config
$this->basis_folder = realpath(dirname(__FILE__)).'/';


if(LOCAL){
  $this->http_basis = '/PAGEOS/';
  $this->http = "/PAGEOS/Investoren/PageOs/";//fuer img und href
}
else{
  $this->http_basis = '/PAGEOS/';
  $this->http = "/PAGEOS/Investoren/PageOs/";//fuer img und href

}

#http_rootroot
$this->http_rootroot = '/';

# http active project
$this->http_project = "/PAGEOS/{$d['name_project_folder']}/";


/*
 * Use GUI
 */
$this->use_gui = true;

# DB
if(LOCAL === false){
  $this->host = 'localhost';
  $this->user = 'web471';
  $this->database = 'usr_web471_2';
  $this->pass = 'yUxpgWpE';
}
else
  $this->database_local = 'all_modules';


/*
 * $this->B->save_standard_fields
 *
 */
$this->save_standard_fields = 'save_user_data';


# EMAIL
$this->Host = "";
$this->SMTPAuth = true;
$this->Username = "";
$this->Password = "";
//$this->Host = "spirituelles-portal.de";
//$this->SMTPAuth = true;
//$this->Username = "web333p3";
//$this->Password = "pwFSmtp";

$this->From = '';
$this->FromName='';
$this->EmailAdmin='';

$this->email_webmaster = 'info@zinstraum.de';
$this->email_betreiber = 'thomas.kratz@web.de';

$this->bank_komplett = '


';

$this->nothing_found = 'Keine Datensätze gefunden';
$this->after_login = "http://{$_SERVER['HTTP_HOST']}/zuladungen";