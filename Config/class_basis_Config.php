<?php

class basis_Config extends basis{
	
	
	function __construct($d){

		/**
		 * Include own config &
		 * a config from basis folder, if exists
		 */
		include realpath(dirname(__FILE__)).'/config.php';
		@include $this->root_project.'config.php';

		/**
		 * Class to convert dates
		 */
		$this->CD = new convert_datum;
		
		/**
		 * Symlinks for this project
		 */
		$this->symlinks();
		
		/**
		 * QE, DEVAS
		 */
		QE::init($this);
		//DEVAS::init($this);
		
		
		parent::__construct($d);
		/**
		 * HIER NIX DRUNTER SCHREIBEN !!!!!!!!!!!!!!
		 */

	}
	
	
	private function symlinks(){

		/**
		 * img Ordner in Frontend
		 */
//		$target = $this->rootroot_symlink.'Frontend/files';
//    $link = $this->rootroot_symlink.'Admin/files';
//
//		if(!is_dir($link))
//		if(!symlink($target, $link)){
//			if(LOCAL)
//			echo "<span style='color: red'><br>TEST :: ".basename(__FILE__)." (".__LINE__.") -> </span>"; echo
//			"ERROR: Creating Symlink";
//	 }
	}
	
	
	function datum_termin($d){		
		
		$date = $this->CD->convert($d->von,"d");
		
		if($d->bis=="1989-01-01" or $d->bis=="0000-00-00")
		$datum="am $date";
		
		else{
			$datum =  $this->CD->convert($d->von,"t") . "." .
								$this->CD->convert($d->von,"m") . "." .
								"-" .
								$this->CD->convert($d->bis,"t") . "." .
								$this->CD->convert($d->bis,"m") . "." .
								$this->CD->convert($d->bis,"j2");
		}
	  
		return $datum;
	}
	
	function check_login(){
		return "not";
	}
}