<?php

//$this->noCheckLogin_config = true;
$this->tpl = $this->root_project.'templates/';
$this->http_project = '/';
$this->http = '/PageOs/';

# Standard Page
$this->pages_templates['areas_template'] = $this->root_project.'templates/page/page_standard.tpl';
$this->pages_templates['final_template'] = $this->rootroot.'Config/templates/page/frame.tpl';

# Logon
$this->login_templates['areas_template'] = $this->root_project.'templates/page/login.tpl';
$this->login_templates['final_template'] = $this->rootroot.'Config/templates/page/frame.tpl';

# Print Page
$this->print_templates['areas_template'] = $this->root_project.'templates/page/print.tpl';
$this->print_templates['final_template'] = $this->rootroot.'Config/templates/page/frame_print.tpl';