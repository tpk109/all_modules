<?php
  include $this->B->root_project.'/templates/print/print_head.tpl';



?>

<table>
  <tr class="top">
    <td class="print_title">{{print_title}}</td>
    <td>{{client_name}}</td>
    <td>{{row->date}}</td>
  </tr>

  <tr class="top">
    <td class="label">Mitarbeiter:</td>
    <td colspan="2">{{employee_name}} ({{row->employees_id}})</td>
  </tr>

  <tr class="top">
    <td class="label">Standplatz:</td>
    <td colspan="2">{{row->locations_id}}</td>
  </tr>

  <tr class="list">
    <td colspan="3">
      {{list}}
    </td>
  </tr>
</table>

<script>window.print();</script>