<?php 


?>

<div class='form_1 box row'>
  <div class='col-sm-26'>    
      {{ROW->text->value}}
  </div>
    <div class='col-sm-10'>
      
      <div class="row">
        <div class="col-sm-36">
           <div class='files_id {{ROW->files_id->class_td}}' json='{{ROW->files_id->json}}'>{{ROW->files_id->value}}</div>
        </div>
      </div>
  
      <div class='row'>
        <div class='col-sm-10 form_label'>
          <span>Alt: </span>
        </div>
        <div class='col-sm-26'>
          {{ROW->img_alt->value}}
        </div>
      </div>
      
      <div class='row'>
        <div class='col-sm-10 form_label'>
          <span>Bild rechts: </span>
        </div>
        <div class='col-sm-26'>{{ROW->img_pos->value}}</div>
      </div>
      
      <div class='row'>
        <div class='col-sm-10 form_label'>
          <span>YouTubeID: </span>
        </div>
        <div class='col-sm-26'>
          {{ROW->youtube_id->value}}
        </div>
      </div>
      
      <div class='row'>
        <div class='col-sm-10 form_label'>
          <span>Offline: </span>
        </div>
        <div class='col-sm-26'>
          {{ROW->offline->value}}
        </div>
      </div>
      
      <div class='row'>
        <div class='col-sm-10 form_label'>
          <span>Löschen: </span>
        </div>
        <div class='col-sm-26'>
          {{ROW->delete}}
        </div>
      </div>
      
      <div class='row'>
        <div class='col-sm-10 form_label'>
          <span>Sortieren: </span>
        </div>
        <div class='col-sm-26'>
          {{ROW->moving}}
        </div>
      </div>
      
    </div>
  </div>
</div>