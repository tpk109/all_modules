<?php  
  $teaser_img = QE::img_path($ROW->files_id->value);
  
  # Show More?
  $sql = "SELECT `id` FROM `cms_texts` WHERE `blog_items_id`='{$ROW->id->value}'";
  $more = (DB::num_rows_sql($sql) > 0)? "<a class='more' href='/BE/blog/{$ROW->url->value}'>Mehr</a>" : '';
  $href = ($more != '')? "/BE/blog/{$ROW->url->value}" : '';
?>

<div  class='row teaser blog_items'>
  <div class='columns small-12'>
    <a href='<?php echo $href; ?>'><h2><span role='presentation'>{{ROW->title->value}}</h2></span></a>
  </div>
  <div class='columns small-12 medium-4'>
    <a href='<?php echo $href; ?>'>
      <img class='teaser_img' src='<?php echo $teaser_img; ?>' alt='{{ROW->teaser_img_alt->value}}'>
    </a>
  </div>
  <div class='columns small-12 medium-8'>      
    {{ROW->teaser->value}}
    <?php echo $more; ?>
  </div>
</div>