<div class='img_list box form_1 col-sm-36'>
  <div class="row">
    <div class="col-sm-36">
      <div class='img_wrap'>
        <div class='files_id {{ROW->files_id->class_td}}' json='{{ROW->files_id->json}}'>{{ROW->files_id->value}}</div>
      </div>
    </div>
  </div>
  
  <div class='row'>
    <div class='col-sm-10 form_label'>
      <span>Online: </span>
    </div>
    <div class='col-sm-26'>
      {{ROW->online->value}}
    </div>
  </div>
  
  <div class='row'>
    <div class='col-sm-10 form_label'>
      <span>Auf Index: </span>
    </div>
    <div class='col-sm-26'>
      {{ROW->aufIndex->value}}
    </div>
  </div>
  
  <div class='row'>
    <div class='col-sm-10 form_label'>
      <span>Editorial: </span>
    </div>
    <div class='col-sm-26'>
      {{ROW->nlAnschreiben->value}}
    </div>
  </div>
  
  <div class='row'>
    <div class='col-sm-10 form_label'>
      <span>Meta-Title: </span>
    </div>
    <div class='col-sm-26'>
      {{ROW->meta_title->value}}
    </div>
  </div>
  
  <div class='row'>
    <div class='col-sm-10 form_label'>
      <span>Meta-Desc: </span>
    </div>
    <div class='col-sm-26' title='{{ROW->meta_description->value_pure}}'>
     {{ROW->meta_description->value}}
    </div>
  </div>
  
  <div class='row'>
    <div class='col-sm-10 form_label'>
      <span>Überschrift: </span>
    </div>
    <div class='col-sm-26' title='{{ROW->titel->value_pure}}'>
      {{ROW->titel->value}}
    </div>
  </div>
  
  <div class='row'>
    <div class='col-sm-10 form_label'>
      <span>Autor: </span>
    </div>
    <div class='col-sm-26' title='{{ROW->autor->value_pure}}'>
      {{ROW->autor->value}}
    </div>
  </div>
  
  <div class='row'>
    <div class='col-sm-10 form_label'>
      <span>Datum: </span>
    </div>
    <div class='col-sm-26'>
      {{ROW->datum->value}}
    </div>
  </div>
  
</div>