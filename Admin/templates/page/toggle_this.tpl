<?php
  if(@$start_closed){
    $display_1 = "style='display: none'";
  }
  else $display_2 = "style='display: none'";
?>

<div class='toggle_big glyphicon glyphicon-minus-sign toggle_child_click cursor' id_toggle_this='<?php echo $id; ?>'></div>
<div class='clearfix'></div>

<div <?php echo @$display_1; ?> id='toggle_first' class='toggle_this_<?php echo $id; ?>'>
  <?php echo $toggle_first; ?>
</div>

<div <?php echo @$display_2; ?> id='toggle_second_<?php echo $id; ?>' class='toggle_this_<?php echo $id; ?>'>
  <?php echo $toggle_second; ?>
</div>
