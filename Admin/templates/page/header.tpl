<?php

$rand_attr = "?attr=" . rand ( 10000 , 99999 );
$nav_3_active = (@$nav_3)? " nav_3_active" : "";
$local = ($B->local)? " local" : '';
$style = (@$_GET['body_class'] != "iframe")? "style='padding-top: 250px;'": "";
?>
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="../../favicon.ico">

    <title>Wagner & Ginster</title>

    <!-- Bootstrap core CSS -->
    <link href="<?php echo $B->http; ?>assets/bootstrap/css/bootstrap.min.css" rel="stylesheet">
    
    <!-- Formit -->
    <link rel="stylesheet" href="<?php echo $B->http; ?>formit/includes/cssTools.css<?php echo $rand_attr; ?>" type="text/css" />
    
    <script src="<?php echo $B->http; ?>formit/includes/jquery.js" type="text/javascript"></script>
    <script src="<?php echo $B->http; ?>assets/ckeditor/ckeditor.js"></script>
    <script src="<?php echo $B->http; ?>formt/includes/ckeditor/adapters/jquery.js"></script>
    <script src="<?php echo $B->http; ?>formit/classes/jsDB/js.js<?php echo $rand_attr; ?>" type="text/javascript"></script>
    <script src="<?php echo $B->http; ?>formit/classes/js.js<?php echo $rand_attr; ?>" type="text/javascript"></script>
    <script src="<?php echo $B->http; ?>assets/lightbox.js<?php echo $rand_attr; ?>" type="text/javascript"></script>
    
    <!-- this project -->
    <script src="<?php echo $B->http; ?>classes/formit_js.js<?php echo $rand_attr; ?>" type="text/javascript"></script>
    <link href="css.css<?php echo $rand_attr; ?>" rel="stylesheet">

    <!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
    <!--[if lt IE 9]>
      <script src="https://oss.maxcdn.com/html5shiv/3.7.2/html5shiv.min.js"></script>
      <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
  </head>

  <body <?php echo $style; ?> role="document" class='<?php echo (@$_GET['body_class'])? $_GET['body_class'] : $B->file; echo " ". @$body_class.$nav_3_active.$local; ?>'>
  