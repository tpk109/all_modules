<div id='main_nav' class="navbar navbar-inverse navbar-fixed-top" role="navigation">
  <div class="container">
    <div class="navbar-header">
      <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target=".navbar-collapse">
        <span class="sr-only">Toggle navigation</span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
        <span class="icon-bar"></span>
      </button>
      <a class="navbar-brand" href="login.php?logout">Logout</a>
    </div>
    <div class="navbar-collapse collapse">
      <ul class="nav navbar-nav">
      <li class="dropdown">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown">Admin<span class="caret"></span></a>
          <ul class="dropdown-menu" role="menu">
            <li><a href="categories_onpage_1.php">Kategorien</a></li>
            <li><a href="user_show_all_rows.php">User</a></li>
            <li><a href="languages_page.php">Sprachen</a></li>
            <li><a href="text_companies_show_all_rows.php">Text Firmen</a></li>
            <li><a href="onpage_tools.php">Onpage Tools</a></li>
            <li><a href="onpage_working_history.php">Onpage Liste</a></li>
            <li><a href="log_on_save.php">User Einträge</a></li>
            <li class="divider"></li>
            
          </ul>
      </li>
        <li><a href='customer'>Kunden</a></li>
        <li class="dropdown active">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown">Kunden<span class="caret"></span></a>
          <ul class="dropdown-menu" role="menu">
            <li><a href="customer_form.php">Neu</a></li>
            <li><a href="customer_show_all_rows.php">Editieren</a></li>
            
            
          </ul>
        </li>
        <li><a href='projects_admins_show_all_rows.php?'>Projekt-Admins</a></li>
        <li><a href='projects_show_all_rows.php?'>Projekte</a></li>
        
        <li class="dropdown">
          <a href="#" class="dropdown-toggle" data-toggle="dropdown">Backlinks<span class="caret"></span></a>
          <ul class="dropdown-menu" role="menu">
            <li><a href='backlinks_categories_page.php'>Kategorien</a></li>
            <li><a href='backlinks_websites_for_entries_page.php?type=FORUM'>Foren, etc & Branchenbücher anlegen</a></li>
            <li><a href='backlinks_page.php?type=FORUM'>Backlinks</a></li>            
          </ul>
        </li>
        
        <li><a href='onpage_show_all_rows.php'>OnPage</a></li>
        <!-- <li><a href='text_management_show_all_rows.php'>Texte</a></li> -->
        <li><a href='CP_index.php'>Kunden-Verwaltung</a></li>
        
      </ul>
    </div><!--/.nav-collapse -->
  </div>
</div>