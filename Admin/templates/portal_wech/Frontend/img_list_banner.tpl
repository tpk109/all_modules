<?php 
    //echo "<span style='color: red'>".basename(__FILE__)." (".__LINE__.")</span><br><pre>";
    //print_r($ROW);
    //echo "</pre><br>";

?>
<div class='img_list box form_1 col-sm-10'>
  <div class="row">
    <div class="col-sm-36">
      <div class='img_wrap'>
        {{ROW->img_path->value}}
      </div>
      {{ROW->delete}}
    </div>
  </div>
  
  <div class='row'>
    <div class='col-sm-10 form_label'>
      <span>Link: </span>
    </div>
    <div class='col-sm-26'>
      {{ROW->field_1->value}}
    </div>
  </div>
  
  <div class='row'>
    <div class='col-sm-10 form_label'>
      <span>Notiz: </span>
    </div>
    <div class='col-sm-26' title='{{ROW->field_2->value_pure}}'>
     {{ROW->field_2->value}}
    </div>
  </div>
  
  <div class='row'>
    <div class='col-sm-10 form_label'>
      <span>Von: </span>
    </div>
    <div class='col-sm-26'>
     {{ROW->field_3->value}}
    </div>
  </div>
  
  <div class='row'>
    <div class='col-sm-10 form_label'>
      <span>Bis: </span>
    </div>
    <div class='col-sm-26'>
     {{ROW->field_4->value}}
    </div>
  </div>
  
  <div class='row'>
    <div class='col-sm-10 form_label'>
      <span>Offline: </span>
    </div>
    <div class='col-sm-26'>
     {{ROW->field_6->value}}
    </div>
  </div>
  
  <div class='row'>
    <div class='col-sm-10 form_label'>
      <span>KA*: </span>
    </div>
    <div class='col-sm-26'>
     {{ROW->field_5->value}}
    </div>
  </div>
  
  <div class='row'>
    <div class='col-sm-10 form_label'>
      <span>Sortieren: </span>
    </div>
    <div class='col-sm-26'>
     {{ROW->moving}}
    </div>
  </div>
  
  <div class="row">
    <div class="col-sm-36">
      * Keine Anzeige
    </div>
  </div>
</div>

