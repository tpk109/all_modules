<?php


# error
error_reporting(E_ALL);
ini_set('display_errors', TRUE);


/**
 * Edit this
 */
$d['basis_folder'] = str_replace("\\", "/", realpath(dirname(__FILE__).'/../').'/Config/');
$d['no_session'] = false;
$d['noCheckLogin'] = false;
$d['use_controller'] = true;

/**
 * Don't edit
 */
$name_project_folder = basename(dirname(__FILE__));
$root_project = realpath(dirname(__FILE__)).'/';

define('NAME_PROJECT_FOLDER', $name_project_folder);
define('ROOT_PROJECT', $root_project);
include $root_project.'/autoload.php';

// ATTENTION: New attr also in ajax_Frontend?
$d['name_project_folder'] = $name_project_folder;
$d['root_project'] = $root_project;
$classname = "basis_$name_project_folder";

if(!@$no_new) // included from class_ajax
$B = new $classname($d);
