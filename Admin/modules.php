<?php

trait modules_controller{
  use
    controller_admin_user,
    controller_crm,
    controller_po_gui,
    controller_shop,
    controller_translator;
}


trait modules_data_objects{
  use
    data_objects_standards,
    data_objects_admin_user,
    data_objects_crm,
    data_objects_po_gui,
    data_objects_shop,
    data_objects_translator;
}


trait modules_handmade{
  use
    handmade_po_gui;
}

trait modules_QE{
  use
    QE_po_gui;
}