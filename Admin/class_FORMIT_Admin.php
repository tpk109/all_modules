<?php
class FORMIT_Admin{
  static $DO = "";
  static $B = '';
  public static function init($Object_do_list){
    self::$DO = $Object_do_list;
    self::$B = self::$DO->B;
  }

//  static function blog__main_image_list_draggable($d){
//
//    $ROW = $d['ROW'];
//
//    $fieldname = $d['FIELD_NAME'];
//    $src = $d['ROW']->$fieldname;
//
//    # get thumbs inside
//    $src = self::$DL->uploadDir_http.'/thumbs/'.basename($src);
//
//    $img_path = self::$DL->input_instead($d);
//
//    $html = "
//      <div class='img'><img class='draggable' src='$src' val='{$d['id']}'></div>
//      <div class='img_path_container'>$img_path</div>
//    ";
//
//    return $html;
//  }

  static function zuladungen__formular__insert_vehicle_loadings_data(){
    $vehicle_loadings_id = self::$B->ID_SAVED_DATA;

    /*
     * Get vehicles_id
     */
    $sql = "SELECT `vehicles_id` FROM `vehicle_loadings` WHERE `id`='$vehicle_loadings_id' ORDER BY `id` DESC LIMIT 1";
    $vehicles_id = DB::vf($sql);

    /*
     * Get active articles
     */
    $sql = "SELECT * FROM articles WHERE `active`='1' ORDER BY `sorting`";

    $ROWS = DB::data_rows_o($sql);

    /*
     * Get the last vehicle_loadings with this vehicle
     * Careful: LIMIT 1,1 = second of result (first = 0), as form is saved before we come here!!
     */
    $sql = "SELECT `id` FROM `vehicle_loadings` WHERE `vehicles_id`='$vehicles_id' ORDER BY `id` DESC LIMIT 1,1";
    $last_vehicle_loadings_id = DB::vf($sql);

    foreach ($ROWS as $row) {

      # Get vehicle_loadings_data with stockdata from vehicle
      $sql = "
        SELECT 
          `stock_evening` 
        FROM 
          `vehicle_loadings_data`
          WHERE 
            `vehicle_loadings_id`='$last_vehicle_loadings_id'
          AND 
            `articles_id`='$row->id'";
      $stock_morning = DB::vf($sql);

      # float gemurkse
      $stock_morning = ($stock_morning > 0)? $stock_morning : "NULL";

      $sql = "
        INSERT INTO 
          `vehicle_loadings_data` (
            `vehicle_loadings_id`,
            `articles_id`,
            `stock_morning`
          ) 
        VALUES(
          '$vehicle_loadings_id',
          '$row->id',
          $stock_morning
        )
      ";
      DB::query($sql);
    }

  }


  static function save_user_data($d){
    
    $table = self::$DO->table;
    $uid = $_SESSION['uid'];
    $insert_id = @self::$B->ID_SAVED_DATA;
    $update_id = $d['update_id'];
    
    if($d['insert_update'] == 'insert')
      $sql = "
        UPDATE `$table` SET
          `created_from`='$uid' WHERE
          `id`='$insert_id'
      ";
    else
      $sql = "
        UPDATE `$table` SET
          `changed_by`='$uid',
           `last_change`= CURRENT_TIMESTAMP WHERE
          `id`='$update_id'
      ";

    DB::query($sql);
  }
}
