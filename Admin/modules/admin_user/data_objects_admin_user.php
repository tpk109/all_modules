<?php
trait data_objects_admin_user{


  function admin_user_to_user_group(){
    $DO = new two_tables_table(__FUNCTION__, $this->B);
    $DO->table_v = 'po_user';
    $DO->field_to_show_v = 'email';
    $DO->table_h = 'po_user_groups';
    $DO->field_to_show_h = 'group';
    $DO->type = 'radio';
    $DO->no_value_h = true;
    $DO->init();

    return $DO->return_DO($this->i_want_an_array);
  }


  function admin_user_group_to_page(){
    $DO = new two_tables_table(__FUNCTION__, $this->B);
    $DO->table_v = 'po_admin_pages';
    $DO->field_to_show_v = 'name';
    $DO->table_h = 'po_user_groups';
    $DO->field_to_show_h = 'group';
    $DO->type = 'checkbox';
    $DO->no_value_h = true;
    $DO->init();

    return $DO->return_DO($this->i_want_an_array);
  }


  function navigation_admin(){
    $DO = new do_navigation(__FUNCTION__, $this->B);

    $DO->table = 'po_admin_pages';
    $DO->show_fields = array('only', 'id','name');
    $DO->format = 'bootstrap';
    $DO->template = 'templates/stuff/bootstrap_navbar.tpl';
    $DO->tools = false;

    $DO->init();

    return $DO->return_DO($this->i_want_an_array);
  }


  function admin_seiten_list(){
    $DO = new do_navigation(__FUNCTION__, $this->B);

    $DO->table = 'po_admin_pages';
    $DO->show_fields = array('only', 'id','name');
    $DO->enable_moving = true;
    $DO->enable_child = true;
    $DO->enable_delete = true;
    $DO->format = 'many_level';
    $DO->template = 'templates/stuff/admin_nav.tpl';
    $DO->tools = true;
    $DO->sorting = true;

    $DO->enable_edit = true;
    $DO->edit_per_ajax = true;
    $DO->form_to_edit_ajax = 'admin_seiten_formular';


    $DO->init();

    return $DO->return_DO($this->i_want_an_array);
  }


  public function admin_seiten_formular(){
    $DO = new FORMIT(__FUNCTION__, $this->B);

    $DO->table = 'po_admin_pages';
    $DO->show_fields = array('only', 'name', 'url');
    $DO->go_to = $_SERVER['SCRIPT_NAME'];
    $DO->forms_page = $_SERVER['SCRIPT_NAME'];
    /* $DO->show_fields_layout = array(
      array('LEFT'=>array('only', 'ckeditor_simple')),
      array('RIGHT' => array('only', 'id', 'firstname', 'lastname')),
      array('WOANDERS' => array('only', 'id', 'checkbox', 'select'))
    );//,'ckeditor_simple' */
    $DO->do_this_after_save_ajax = array('reload_do' => array('admin_seiten_list'));
    $DO->do_this_after_save_always = array('sluggify' => array('name' => 'url'));
    $DO->ajax_form = true;
    $DO->new_data_button = true;

    $DO->init();

    return $DO->return_DO($this->i_want_an_array);
  }


  function admin_user_list(){

    $DO = new do_list(__FUNCTION__, $this->B);

    $DO->table = 'po_user';
    $DO->css_class = 'scroll';
    $DO->show_fields = array('only', 'id', 'vorname', 'nachname', 'email');
    $DO->special_class = array('firstname' => 'red');
    $DO->count = false;
    //$DO->html_before = array('firstname' => 'Hallo');
    $DO->html_end = 'world';
    //$DO->func_before = array('lastname' => 'lastname');
    $DO->func_end = 'this_is_the_end';
    $DO->search = true;
    //$DO->search_in_fields = array('firstname');
    $DO->legend = true;
    $DO->sortable = true;
    $DO->enable_edit = true;
    $DO->edit_per_ajax = true;
    $DO->form_to_edit_ajax = 'admin_user_formular';
    $DO->enable_delete = true;
    $DO->enable_moving = false;
    $DO->pagination = true;
    $DO->limit_nr = 33;
    $DO->order_by = 'id DESC';
    $DO->enable_checkboxes = false;

    $DO->init();

    return $DO->return_DO($this->i_want_an_array);

  }


  public function admin_user_formular(){
    $DO = new FORMIT(__FUNCTION__, $this->B);

    $DO->table = 'po_user';
    $DO->show_fields = array('only', 'vorname','nachname','email', 'pw', 'pw_2');
    $DO->go_to = $_SERVER['SCRIPT_NAME'];
    $DO->forms_page = $_SERVER['SCRIPT_NAME'];
    /* $DO->show_fields_layout = array(
      array('LEFT'=>array('only', 'ckeditor_simple')),
      array('RIGHT' => array('only', 'id', 'firstname', 'lastname')),
      array('WOANDERS' => array('only', 'id', 'checkbox', 'select'))
    );//,'ckeditor_simple' */
//    $DO->form_layout = "
//    <table>
//      <tr><td>[[LEFT]]</td><td>[[RIGHT]]</td></tr>
//      <tr><td>[[WOANDERS]]</td><td></td></tr>
//    </table>
//    ";
    $DO->do_this_after_save_ajax = array('reload_do' => array('admin_user_list'));
    $DO->do_this_after_save_always = array('hash_it'=>array('hash_from_this'=>'pw_2', 'hash_into_this'=>'pw'));
    $DO->ajax_form = true;
    $DO->new_data_button = true;
    $DO->empty_fields = array('pw', 'pw_2');

    $DO->init();

    return $DO->return_DO($this->i_want_an_array);
  }


  function admin_user_groups_list(){

    $DO = new do_list(__FUNCTION__, $this->B);

    $DO->table = 'po_user_groups';
    $DO->show_fields = array('only', 'group', 'admin', 'developer');
    $DO->special_class = array('firstname' => 'red');
    $DO->count = 'wieviele?';
    //$DO->html_before = array('firstname' => 'Hallo');
    $DO->html_end = 'world';
    //$DO->func_before = array('lastname' => 'lastname');
    $DO->func_end = 'this_is_the_end';
    $DO->search = true;
    $DO->search_in_fields = array('firstname');
    $DO->legend = true;
    $DO->sortable = true;
    $DO->enable_edit = true;
    $DO->edit_per_ajax = true;
    $DO->enable_delete = true;
    $DO->enable_moving = false;
    $DO->pagination = true;
    $DO->limit_nr = 33;
    $DO->order_by = 'id DESC';
    $DO->enable_checkboxes = false;
    $DO->form_to_edit_ajax = 'admin_user_groups_formular';

    $DO->init();

    return $DO->return_DO($this->i_want_an_array);

  }


  public function admin_user_groups_formular(){
    $DO = new FORMIT(__FUNCTION__, $this->B);

    $DO->table = 'po_user_groups';
    $DO->show_fields = array('only', 'group', 'admin', 'developer', 'users_index');
    $DO->go_to = $_SERVER['SCRIPT_NAME'];
    $DO->forms_page = $_SERVER['SCRIPT_NAME'];
    $DO->do_this_after_save_ajax = array('reload_do' => array('admin_user_groups_list'));
    $DO->ajax_form = true;
    $DO->new_data_button = true;

    $DO->init();

    return $DO->return_DO($this->i_want_an_array);
  }

}