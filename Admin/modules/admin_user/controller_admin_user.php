<?php

trait controller_admin_user{

  function user_rights($check_from_outside = false){

    $sql = "SELECT `po_user_groups_id` FROM `po_user` WHERE `id`='" . @$_SESSION['uid'] . "'";
    $user_groups_id  = DB::vf($sql);

    /*
     * Admin or normal user
     */
    if(@$_SESSION['admin'] || @$_SESSION['developer']){
      $this->B->user_no_right_for_page = false;
      if($check_from_outside)
        return true;
    }
    else{
      $url = ($this->UV[1] == '') ? 'index' : $this->UV[1];
      $url = ($check_from_outside)? $check_from_outside : $url;
      $sql = "SELECT `id` FROM `po_admin_pages` WHERE `url`='$url'";
      $admin_pages_id = DB::vf($sql);
      $sql = "
      SELECT `id` FROM po_id_to_id WHERE
      `group_identifier`='po_admin_pages' AND
      `id_1`='$admin_pages_id' AND
      `id_2`='$user_groups_id'";

      DB::num_rows_sql($sql);

      /*
       * Page == '' -> Right for called Page
       * Otherwise call from e.g. class_do_navigation
       */
      if(!$check_from_outside){
        if (DB::num_rows_sql($sql) == 0)
          $this->B->user_no_right_for_page = true;
        else
          $this->B->user_no_right_for_page = false;
      }
      # from class_do_navigation
      else{
        if (DB::num_rows_sql($sql) == 0)
          return false;
        else
          return true;
      }
    }
  }


  function user_has_no_right(){
    include $this->B->root.'lib/simple_vars.php';

    $P->main = "
      <h1>Kein Recht für diese Seite</h1>
      Sie müssen für diese Seite freigeschaltet werden.<br>
      Bitte an den Admin wenden.
    ";
    return $html = $P->show_page($this->B->pages_templates);
  }


  function login(){
    include $this->B->root.'lib/simple_vars.php';

    /**
     * Logout
     */
    if(@$this->UV[2] == 'logout')
    {
      $this->B->session_destroy();
      $message = "<div class='message_box'>Ihre Arbeitssitzung wurde beendet!</div>";
    }

    #login checken
    if(isset($_POST['Login']))
    {
      $message = $id_attribute = "";

      if($_POST['User'] == "")
        $message = "Bitte das Feld <b>Login</b> ausf&uuml;llen<br>";

      if($_POST['Password'] == "")
        $message.="Bitte das Feld <b>Passwort</b> ausf&uuml;llen<br>";

      if($message == "")
      {
        $Password = DB::vf("SELECT `pw` FROM `po_user` WHERE `email`='" . $_POST['User'] . "'");

        if($Password == md5($_POST['Password'])){

          $id = DB::vf("SELECT `id` FROM `po_user` WHERE `email`='" . $_POST['User'] . "'");
          $_SESSION['uid'] = $id;
          $_SESSION['SID'] = uniqid();

          /*
           * Admin or Developer?
           */
          # User Group
          $sql = "SELECT `po_user_groups_id` FROM `po_user` WHERE `id`='" . @$_SESSION['uid'] . "'";
          $user_groups_id  = DB::vf($sql);
          # Admin?
          $sql = "SELECT * FROM `po_user_groups` WHERE `id`='$user_groups_id'";
          $row__user_group = DB::data_rows_o_0($sql);
          if($row__user_group->admin == 1)
            $_SESSION['admin'] = true;
          # Developer?
          $sql = "SELECT `developer` FROM `po_user_groups` WHERE `id`='$user_groups_id'";
          if(DB::vf($sql) == 1)
            $_SESSION['developer'] = true;

          /*
           * Users_index
           */
          $sql = "SELECT `url` FROM `po_admin_pages` WHERE `id`='$row__user_group->users_index'";
          $users_index = (DB::vf($sql))? DB::vf($sql) : 'index';

          # Bang
          header("Location: /$users_index");
        }

        else
          $message="Die Anmelde-Daten sind nicht korrekt<br>";
      }

      $message = "<div class='message_box'>$message</div>";

    }


    $P->TD->message = @$message;

    $P->main = $P->template_it($this->B->root_project.'templates/stuff/login.tpl');

    return $html = $P->show_page($this->B->login_templates);
  }


  function user(){

    include $this->B->root.'lib/simple_vars.php';

    $P->TD->content = '<h1>Userverwaltung</h1>';
    $P->main = $P->template_it($this->B->tpl_bootstrap_3.'cols_1.tpl');

    # User
    $P->TD->row_class = 'box_1';
    $P->TD->title = 'User';
    $P->TD->left = $dos->admin_user_list();
    $P->TD->right = '<h2>User Daten</h2>'.$dos->admin_user_formular();
    $P->main.= $P->template_it($this->B->tpl_bootstrap_3.'cols_1_1.tpl');

    # User Groups
    $P->TD->row_class = 'box_1';
    $P->TD->title = 'User Gruppen';
    $P->TD->left = $dos->admin_user_groups_list();
    $P->TD->right = $dos->admin_user_groups_formular();
    $P->main.= $P->template_it($this->B->tpl_bootstrap_3.'cols_1_1.tpl');

    # User to page
    $P->TD->row_class = 'box_1';
    $P->TD->title = 'Seitenrechte von User Gruppe';
    $P->TD->content = '<h2>GruppGrupp</h2>'.$dos->admin_user_group_to_page();
    $P->main.= $P->template_it($this->B->tpl_bootstrap_3.'cols_1.tpl');

    # User to User Group
    $P->TD->row_class = 'box_1';
    $P->TD->title = 'User Zuordnung zur User Gruppe';
    $P->TD->content = $dos->admin_user_to_user_group();
    $P->main.= $P->template_it($this->B->tpl_bootstrap_3.'cols_1.tpl');


//    $P->main = $P->template_it($this->B->tpl_bootstrap_3.'cols_1_2.tpl');
//
//    $P->main.= '<h2>User Gruppen</h2>'.$dos->admin_user_groups_list();
//    $P->main.= $dos->admin_user_groups_formular();
//
//    $P->main.= $dos->admin_user_to_user_group();
//
//    $P->main.= '<h2>Seitenrechte</h2>'.$dos->admin_user_group_to_page();

    return $html = $P->show_page($this->B->pages_templates);
  }


  function seiten_admin(){

    include $this->B->root.'lib/simple_vars.php';

    //$P->main.= $dos->admin_user_to_user_group();
    //
    //$P->main.= '<h2>Seitenrechte</h2>'.$dos->admin_user_group_to_page();
    $P->main.= '<h2>Seiten</h2>'.$dos->admin_seiten_list();
    $P->main.= $dos->admin_seiten_formular();

    return $html = $P->show_page($this->B->pages_templates);
  }


}
