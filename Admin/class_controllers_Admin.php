<?php

class controllers_Admin extends controller_pageos{
  use modules_controller;
  function __construct($d){
    
    parent::__contstruct($d);
    
    $this->P = $d['P'];
    
    /**
     * Check User Rights
     * Only check if locked in
     */
    if(@!$this->B->check_login_failed && $this->UV[1] != 'login' && !$this->B->no_db_selected)
    $this->user_rights();
    
    
  }

  
  function four04(){
    include $this->B->root.'lib/simple_vars.php';
    echo "<br>#|#$ 404";
  }


  # DOMAINS DOMAINS DOMAINS

  function domains(){

    include $this->B->root.'lib/simple_vars.php';

    $P->main.= "
      {$dos->domains()}
    ";

    return $html = $P->show_page($this->B->pages_templates);
  }


  function todo(){
    include $this->B->root.'lib/simple_vars.php';

    $P->TD->left = "
      {$dos->todo__list()}
    ";

    $P->TD->right = "    
      {$dos->todo__formular_new()}
    ";

    $P->main = $P->template_it($this->B->tpl_bootstrap_3.'cols_1_1.tpl');

    $P->main.= "
      {$dos->todo__formular_text()}
    ";
    return $html = $P->show_page($this->B->pages_templates);
  }




  function gesk(){
    include $this->B->root.'lib/simple_vars.php';

    $nicht_gesendete_mails = array(array('EMAIL', 'NAME', 'VORNAME', 'PLZ', 'ORT'));
    /*
     * Öffne gesendete und ab in array
     */
    if (($handle = fopen("gesendet.csv", "r")) !== FALSE) {
      while (($data = fgetcsv($handle, 1000, ";")) !== FALSE) {

          //echo $data[0] . "<br />\n";
          $gesendete_mails[] = strtolower($data['0']);

      }
      fclose($handle);
    }

    /*
     * Öffne alle Emails von richtiger Datei
     */
    $i = 0;
    $leer = 0;
    if (($handle = fopen("kunden.csv", "r")) !== FALSE) {
      while (($data = fgetcsv($handle, 1000, ";")) !== FALSE) {
//    echo "<span style='color: red'>".basename(__FILE__)." (".__LINE__.")</span><br><pre>";
//        print_r($data);
//        echo "</pre><br>";
        $gesperrt = false;
        foreach($data as $field){
          if(strstr(strtolower($field), 'gesperrt'))
            $gesperrt = true;
        }


        if(@$gesperrt)
          $i++;
        if($data['1'] == '')
          $leer++;
        # wenn email nicht in gesendete_mails enhalten ist, speichere mail in array
//        if(
//          !in_array(strtolower($data[1]), $gesendete_mails) &&
//          !$gesperrt &&
//          $data[6] > 19999 &&
//          $data[6] < 60000
//        )
//          $nicht_gesendete_mails[] = array($data[1], $data[2], $data[5], $data[6],$data[7]);

        # wenn email nicht in gesendete_mails enhalten ist, speichere mail in array
        if(
          !$gesperrt
        )
          $nicht_gesendete_mails[] = array($data[1], $data[2], $data[5], $data[6],$data[7]);
      }
      fclose($handle);
    }

//        echo "<span style='color: red'>".basename(__FILE__)." (".__LINE__.")</span><br><pre>";
//            print_r($nicht_gesendete_mails);
//            echo "</pre><br>";

    $fp = fopen('kunden_ohne_gespert.csv', 'w');

    foreach ($nicht_gesendete_mails as $fields) {
      fputcsv($fp, $fields, ';');
    }

    fclose($fp);



    $P->TD->content = "
      $i
      <br><br>
      $leer
    ";

    $P->main = $P->template_it($this->B->tpl_bootstrap_3.'cols_1.tpl');


    return $html = $P->show_page($this->B->pages_templates);
  }

  
  
}
