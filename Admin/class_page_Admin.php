<?php


	/**
	 * page short summary.
	 *
	 * page description.
	 *
	 * @version 1.0
	 * @author Thomas_2
	 */
	class page_Admin extends page
	{
		public $TD;
		
		public $left = '';
		public $main = '';
		public $right = '';
		public $below = '';
		public $field_name;
		
		
		function __construct($d = ''){
			$this->B = $d['B'];
			$this->frame = (@$d['frame'])? $d['frame'] : 'frame';
			
			/**
			* template data
			*/
			$this->TD = (object)array();
			
			/**
			* Data objects
			*/
			$this->dos = $this->B->dosse();
		}
		
		
		/**
		* TEMPLATE IT
		*/
		function template_it($file){		
							
			foreach($this->TD as $key =>$val){
				$$key = $val;
			}	
				
			ob_start();
			include $file;
			$html = ob_get_clean();
		
			# Replace {{var}}
			foreach($this->TD as $key => $val){
				//echo "<br>#|#$key";
				//echo "<br>#|#$val";
				//echo "<br><pre>KEY:";
				//print_r($key);
				//echo "</pre><br>VAL:";
				//echo "<br><pre>";
				//print_r($val);
				//echo "</pre><br>";
				if(!is_array($val) && !is_object($val))
				$html = str_replace("{{{$key}}}", $val, $html);
				
				elseif(is_object($val))
					foreach($val as $objects_field => $objects_value){
						$html = str_replace("{{" . $key . "->" . $objects_field . "}}", $objects_value, $html);
					}
			}
			
		
			# Delete not replaced vars
			$html = preg_replace('/{{[\s\S]*?}}/', '', $html, -1 );
			
			return $html;
		
			$this->TD = ''; 
		}
		
		
		public function to_json($j) {
			$json = array();
			foreach($j as $key => $value) {
					$json[$key] = $value;
			}
			return json_encode($json);
		}
		
		
		function show_page($template){
			
			
      include $this->B->root.'lib/simple_vars.php';
			
			$areas_template = $template['areas_template'];
			$final_template = $template['final_template'];
			
			foreach($this as $key=>$val){
				if(!is_array($val) && !is_object($val))
			  $areas[$key] = $val;
			}
			
			/**
			 * Navigation
			 */
			if(strstr($areas_template, 'page_standard.tpl') != '')
			  $areas['main_nav'] = $dos->navigation_admin();

			/*
			 * Developers stuff
			 */
			if(@$_SESSION['developer'])
			  $content['developer'] = $this->developer_tools();

			
			$content['html'] = $this->B->getContent($areas_template, $areas);

			/**
			 * json_configs for js.js and ajax_base.php
			 */
			$content['json_configs'] = $this->B->json_configs();
			
			$content['this_css'] = (@$template['this_css'])?"<link href='{$template['this_css']}' rel='stylesheet'>" : '';
			$content['iframe'] = @$template['iframe'];

			
			echo $this->B->getContent($final_template, $content);
			
		}
	}
		
