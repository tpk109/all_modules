<?php

class data_objects_Admin
{
  use modules_data_objects;

  function __construct($d)
  {
    $this->B = $d['B'];
    $this->i_want_an_array = @$d['i_want_an_array'];
  }


  # DOMAINS DOMAINS DOMAINS

  function domains()
  {

    $DO = new do_list(__FUNCTION__, $this->B);

    $DO->table = 'domains';
    $DO->css_class = 'list_select scroll';
    $DO->show_fields = array('only', 'domain', 'data');
    $DO->special_class = array('firstname' => 'red');
    $DO->count = '';
    //$DO->html_before = array('firstname' => 'Hallo');
    $DO->html_end = 'world';
    //$DO->func_before = array('lastname' => 'lastname');
    $DO->func_end = 'this_is_the_end';
    $DO->search = true;
    $DO->search_in_fields = array('title');
    $DO->legend = true;
    $DO->sortable = false;
    $DO->enable_edit = false;
    $DO->edit_per_ajax = true;
    $DO->enable_delete = true;
    $DO->enable_moving = false;
    $DO->pagination = true;
    $DO->limit_nr = 50;
    $DO->order_by = 'id DESC';
    $DO->enable_checkboxes = false;

    $DO->func_instead = array('data' => 'whois');

    $DO->init();

    return $DO->return_DO($this->i_want_an_array);

  }


  function todo__list()
  {

    $DO = new do_list(__FUNCTION__, $this->B);

    $DO->table = 'todo';
    $DO->css_class = 'list_select scroll';
    $DO->show_fields = array('only', 'title');
    $DO->special_class = array('firstname' => 'red');
    $DO->count = '';
    //$DO->html_before = array('firstname' => 'Hallo');
    $DO->html_end = 'world';
    //$DO->func_before = array('lastname' => 'lastname');
    $DO->func_end = 'this_is_the_end';
    $DO->search = true;
    $DO->search_in_fields = array('title');
    $DO->legend = true;
    $DO->sortable = false;
    $DO->enable_edit = false;
    $DO->edit_per_ajax = true;
    $DO->row_clickable = array(
      'just_do_this' => array(
        'reload_do' => array(
          'todo__formular_text'
        )
      )
    );
    $DO->enable_delete = true;
    $DO->enable_moving = false;
    $DO->pagination = true;
    $DO->limit_nr = 50;
    $DO->order_by = 'id DESC';
    $DO->enable_checkboxes = false;
    $DO->input_instead = array('title', 'text', 'text_2');

    $DO->init();

    return $DO->return_DO($this->i_want_an_array);

  }


  public function todo__formular_new()
  {

    $DO = new FORMIT(__FUNCTION__, $this->B);

    $DO->edit_id = (@$this->B->ajaxed_json->id) ? $this->B->ajaxed_json->id : '';

    if (@$this->B->ajaxed_json->form_submitted)
      $foo = 'baz';

    $DO->table = 'todo';
    $DO->do_title = '';
    $DO->css_class = 'list_select';
    $DO->show_fields = array('only', 'title');
    $DO->do_this_after_save_ajax = array('reload_do' => array('todo__list'));
    $DO->ajax_form = true;
    $DO->insert_content_after_save = false;
    $DO->new_data_button = false;

    $DO->init();

    return $DO->return_DO($this->i_want_an_array);
  }


  public function todo__formular_text()
  {

    $id = (@$this->B->ajaxed_json->id) ? $this->B->ajaxed_json->id : '';

    $DO = new FORMIT(__FUNCTION__, $this->B);

    if ($id)
      $DO->edit_id = $id;
    if (@$this->B->ajaxed_json->form_submitted)
      $foo = 'baz';
    else
      $DO->no_form_fields = true;

    $DO->table = 'todo';
    $DO->do_title = '';
    $DO->css_class = 'list_select';
    $DO->show_fields = array('only', 'text');
    $DO->do_this_after_save_ajax = array('reload_do' => array('todo__list'));
    $DO->ajax_form = true;
    $DO->insert_content_after_save = false;
    $DO->new_data_button = false;
    $DO->insert_content_after_save = true;

    $DO->init();

    return $DO->return_DO($this->i_want_an_array);
  }


}