<?php

class handmade_Admin{

  use modules_handmade;

  function __construct($do){
    $this->B = $do->B;
    $this->do = $do;
    
    $d['B'] = $this->B;
    $this->dos = new data_objects_Admin($d);
  }
  
  function zeit__summe(){

    $minuten = 0;

    $sql = "SELECT `zeit` FROM `zeit`";

    $ROWS = DB::data_rows_o($sql);

    foreach($ROWS as $row){
      $minuten = $minuten + intval($row->zeit);
    }

    $stunden = $minuten / 60;
    
    $this->do->set_html_for_id("
      
      <div class='reload_this'>
        
        Stunden: $stunden
        
      </div>
    ", true);
  }
  
  
  
}

 ?>

