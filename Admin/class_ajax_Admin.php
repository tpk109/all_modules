<?php
/*
 * TEST
$_REQUEST['func'] = 'one_anbieter';
$_REQUEST['json'] = '{"func": "one_anbieter", "h":600, "w":800, "idAdr":94}';
*/


//require realpath(dirname(__FILE__))."/class_basis_Admin.php";
//$attrs['noCheckLogin'] = true;
//$attrs['name_project_folder'] = 'Frontend';
//$attrs['root_project'] = realpath(dirname(__FILE__).'/../')."/Admin/";
//$attrs['config'] = "Portal";
//$B = new basis_Frontend($attrs);
//
//$aF = new ajax_Frontend($B);
//
//$return = $aF->$_REQUEST['func']();

//echo $return;

class ajax_Admin{
  
  function __construct($DO){
    $this->DO = $DO;
    $this->B = $DO->B;
  }
  
//  function bg__list__choose_bg(){
//
//    $id = $this->B->ajaxed_json->whereVal;
//
//    /**
//     * Set files_1 for all images to 0, except the just clicked one
//     */
//    $sql = "UPDATE `files` SET `field_1`= '0' WHERE `id`!='$id'";
//    DB::query($sql);
//
//    /**
//     * Img Data
//     */
//    $img_data = DB::data_row_o('files', $id);
//
//    /**
//     * Pathinfo of img_server_url
//     */
//    $path_parts = pathinfo($img_data->img_server_url);
//
//    /**
//     * Checkbox auf active img was clicked ??
//     * -> delete active image
//     * else: copy this image to folder activve
//     */
//    if($img_data->field_1 == 0)
//    unlink($path_parts['dirname'].'/active/active.jpg');
//
//    else{
//      /**
//       * Copy this img to acive folder, existing ones are overwritten
//       */
//      $copy_to = $path_parts['dirname'].'/active/active.jpg';
//      copy($img_data->img_server_url, $copy_to);
//    }
//
//  }

  function rueckmeldungen__zuruecksetzen_btn(){

    $vehicles_loadings_id = $this->B->ajaxed_PAGE->rueckmeldungen__vehicle_loadings_list->active_item;

    $sql = "
      UPDATE `vehicle_loadings_data`
        SET
          `sold`= NULL,
          `loss`= NULL,
          `voucher`= NULL,
          `back_to_stock`= NULL,
          `stock_evening`= NULL
        WHERE
          `vehicle_loadings_id`='$vehicles_loadings_id'
    ";

    DB::query($sql);

  }


  function rueckmeldungen__finish_btn(){

    $vehicle_loadings_id = $this->DO->ajaxed_PAGE->rueckmeldungen__vehicle_loadings_list->active_item;

    /*
     * Calculate stuff
     * (stock_evening, sold, VK_sum
     */

    # get data
    $sql = "SELECT * FROM `vehicle_loadings_data` WHERE `vehicle_loadings_id`='$vehicle_loadings_id'";
    $ROW = DB::data_rows_o($sql);

    foreach ($ROW as $data){

        /*
         * Stock Eveneing
         */
      $sold =
        $data->stock_morning +
        $data->stock_added -
        $data->loss -
        $data->back_to_stock -
        $data->stock_evening
      ;

      /*
       * Sold negative Alert
       */
      if($sold < 0){
        $Bezeichnung = QE::articles_name($data->articles_id);
        $this->DO->alert("Negativer Wert bei Artikel '$Bezeichnung'");
      }

      /*
       * VK_sum
       */
      $VK = QE::exep_articles_VK($data->articles_id);
      $VK_sum = $sold * $VK;



      @$multi_sql.= "
        UPDATE 
          `vehicle_loadings_data` 
        SET 
          `sold`='$sold',
          `VK_sum`='$VK_sum'
        WHERE
          `id`='$data->id';
      ";
    }

    DB::multi_query($multi_sql);

    if($vehicle_loadings_id == '')
      $this->DO->alert('Kein Wagen ausgewählt');

    $sql = "
      UPDATE 
        `vehicle_loadings` 
      SET 
        `closed`='1' 
      WHERE 
        `id`='$vehicle_loadings_id';
    ";
    DB::query($sql);
  }

  
}