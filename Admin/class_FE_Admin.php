<?php
class FE_Admin{

  static $DO = "";
  static $B = '';

  public static function init($Object_do_list){
    self::$DO = $Object_do_list;
    self::$B = self::$DO->B;
  }


  static function zuladungen__vehicle_loadings_formular__vehicles_id(){

    # exclude own id
    $id_when_correction = ($_POST['id'] != '')? "`id`!='{$_POST['id']}' AND " : '';

    $sql = "
      SELECT `vehicles_id` FROM `vehicle_loadings` 
        WHERE
          $id_when_correction
          `closed` IS NULL AND
          `vehicles_id`='{$_POST['vehicles_id']}'";

    if(DB::num_rows_sql($sql) > 0)
      return "Wagen <strong>{$_POST['vehicles_id']}</strong> wurde schon zugeladen";
  }


  static function zuladungen__vehicle_loadings_formular__employees_id(){

    # exclude own id
    $id_when_correction = ($_POST['id'] != '')? "`id`!='{$_POST['id']}' AND " : '';

    $sql = "
      SELECT * FROM `vehicle_loadings` 
        WHERE
           $id_when_correction
          `closed` IS NULL AND
          `employees_id`='{$_POST['employees_id']}'";

    if(DB::num_rows_sql($sql) > 0){
      $ROW = DB::data_rows_o_0($sql);

      return "Mitarbeiter <strong>{$_POST['employees_id']}</strong> ist schon Wagen <strong>$ROW->vehicles_id</strong> zugeteilt";}
  }
}